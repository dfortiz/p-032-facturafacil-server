<?php

use Illuminate\Http\Request;
// use Barryvdh\Debugbar\Facade as Debugbar;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: *');
// header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::get('todo', function() {
	// If the Content-Type and Accept headers are set to 'application/json', 
    // this will return a JSON structure. This will be cleaned up later.
    return response(['Product 1', 'Product 2', 'Product 3'],200);
});

Route::get('todo2', 'UtestController@index');

Route::get('articles/{article}', 'ArticleController@show');
Route::get('articles/{id}', 'ArticleController@show');

*/

//Route::post('login', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout');


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



/*Route::group(['middleware' => 'cors' ], function(){
	Route::get('/dologin', function(){

		return "Hello clinets";

	});

});

Route::post('/dologin', ['middleware' => 'cors', function () {
	return ['index2 1111', 'Product 22', 'Product 33'];
}]);	 */

Route::get('/todo3', 
	function(){
		return "Hello clinets";
	});	
// Route::post('/todo3', ['middleware' => 'cors', 'uses' => 'UserController@index2']);	

Route::get('/v1/todo4', ['middleware' => 'cors', 'uses' => 'UtestController@index2']);



/* INVOICE */
Route::post(
	'/v1/invoice/addinvoice', 
	['middleware' => 'cors', 'uses' => 'InvoiceController@addinvoice']);

Route::post(
	'/v1/invoice/listinvoice', 
	['middleware' => 'cors', 'uses' => 'InvoiceController@listinvoice']);


/* /INVOICE */


Route::post('/dologin', ['middleware' => 'cors', 'uses' => 'UserController@validateLogin']);


//Route::get('example', array('middleware' => 'cors', 'uses' => 'ExampleController@dummy'));



/*
Route::group(['middleware' => ['api', 'cors'] ], function(){
    
	//DebugBar::info("Debug: Damir Frank Ortiz");
	// or as a facade
	\Debugger::dump("este es Debugger dump");

    Route::get('/todo3', 'UtestController@index3');
    Route::get('/dologin', 'UtestController@index3');
    //Route::post('/login', 'UtestController@index3');
    //Route::post('/logout', 'UtestController@index3');

}); */



/*Route::group( ['prefix' => 'v1', 'middleware' => ['cors','auth:token'] ], function ()
{
	// Route::get('/todo3', 'UtestController@index2');
	// Route::resource('images', 'ImagesController');
});
*/


/*
Route::group(['middleware' => 'auth:api'], function(){
    // AUTHENTICATED users here,
    // define routes here
});

Route::group(['middleware' => 'guest'], function(){
    // GUEST/UNAUTHORIZED USERS
    // ENLIST routes here
});

api_tokend
*/


/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/