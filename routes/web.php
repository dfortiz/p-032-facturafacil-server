<?php

use Barryvdh\Debugbar\Facade as Debugbar;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: *');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	DebugBar::info("Debug: Damir Frank Ortiz...");
    return view('welcome');
});

Route::get('/getpdf2', function () {
    return view('welcome');
});

Route::get('/getpdf/{iid}/{eid}', 'GenpdfController@GetInvoice2');

//Route::get('/getpdf2', ['middleware' => 'cors', 'uses' => 'GenpdfController@GetPdf']);
//Route::get('/getpdf2', ['uses' => 'GenpdfController@GetPdf']);


// Route::get('/getpdf', ['middleware' => 'cors', 'uses' => 'GenpdfController@GetPdf']);