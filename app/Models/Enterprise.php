<?php

namespace App\Models;

class Enterprise extends \App\Models\Base\Enterprise
{
	protected $fillable = [
		'name',
		'nit',
		'from',
		'from2',
		'from3',
		'from4',
		'logo',
		'ivback',
		'template',
		'state'
	];
}
