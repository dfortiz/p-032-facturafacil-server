<?php

namespace App\Models;

class Role extends \App\Models\Base\Role
{
	protected $fillable = [
		'title'
	];
}
