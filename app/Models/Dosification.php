<?php

namespace App\Models;

class Dosification extends \App\Models\Base\Dosification
{
	protected $fillable = [
		'authorization',
		'year',
		'sectors',
		'dt_registration',
		'dt_start',
		'dt_end',
		'dt_limit',
		'invoice_next',
		'invoice_start',
		'invoice_end',
		'key',
		'sin_msg1',
		'sin_msg2',
		'enterprise_id',
		'branch_id',
		'state'
	];
}
