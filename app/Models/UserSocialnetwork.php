<?php

namespace App\Models;

class UserSocialnetwork extends \App\Models\Base\UserSocialnetwork
{
	protected $fillable = [
		'user_id',
		'socialnetwork_id',
		'username',
		'socialcode'
	];
}
