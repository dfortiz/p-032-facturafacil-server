<?php

namespace App\Models;

class Cli extends \App\Models\Base\Cli
{
	protected $fillable = [
		'clinomb',
		'clifrankdir',
		'clitelf',
		'clistat'
	];
}
