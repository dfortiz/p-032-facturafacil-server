<?php

namespace App\Models;

class LoginAttempt extends \App\Models\Base\LoginAttempt
{
	protected $fillable = [
		'user_id',
		'login_ip',
		'login_time'
	];
}
