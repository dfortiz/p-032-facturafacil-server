<?php

namespace App\Models;

class Socialnetwork extends \App\Models\Base\Socialnetwork
{
	protected $fillable = [
		'name',
		'url',
		'key1',
		'key2',
		'enabled'
	];
}
