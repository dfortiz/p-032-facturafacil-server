<?php

namespace App\Models;

class General extends \App\Models\Base\General
{
	protected $fillable = [
		'name',
		'logo',
		'email',
		'address',
		'phone'
	];
}
