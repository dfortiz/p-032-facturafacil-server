<?php

namespace App\Models;

class Invoice extends \App\Models\Base\Invoice
{
	protected $fillable = [
		'authorization',
		'dosification_id',
		'sector',
		'control_code',
		'dt_emision',
		'key',
		'customer_nit',
		'customer_name',
		'amount',
		'literal',
		'sin_msg1',
		'sin_msg2',
		'qr',
		'user_id',
		'branch_id',
		'state'
	];
}
