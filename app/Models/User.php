<?php

namespace App\Models;

class User extends \App\Models\Base\User
{
	protected $hidden = [
		'password',
		'token',
		'email_token',
		'remember_token'
	];

	protected $fillable = [
		'username',
		'email',
		'password',
		'first_name',
		'last_name',
		'photo',
		'phone',
		'birthday',
		'address',
		'activated',
		'salt',
		'token',
		'email_token',
		'remember_token',
		'register_ip',
		'forget_slug',
		'active_slug',
		'last_login'
	];
}
