<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Invoice
 * 
 * @property int $id
 * @property int $enterprise_id
 * @property string $authorization
 * @property int $dosification_id
 * @property string $sector
 * @property string $control_code
 * @property \Carbon\Carbon $dt_emision
 * @property string $key
 * @property string $customer_nit
 * @property string $customer_name
 * @property float $amount
 * @property string $literal
 * @property string $sin_msg1
 * @property string $sin_msg2
 * @property string $qr
 * @property int $user_id
 * @property int $branch_id
 * @property int $state
 *
 * @package App\Models\Base
 */
class Invoice extends Eloquent
{
	protected $table = 'invoice';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'enterprise_id' => 'int',
		'dosification_id' => 'int',
		'amount' => 'float',
		'user_id' => 'int',
		'branch_id' => 'int',
		'state' => 'int'
	];

	protected $dates = [
		'dt_emision'
	];
}
