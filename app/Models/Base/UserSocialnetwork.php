<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserSocialnetwork
 * 
 * @property int $id
 * @property int $user_id
 * @property int $socialnetwork_id
 * @property string $username
 * @property string $socialcode
 *
 * @package App\Models\Base
 */
class UserSocialnetwork extends Eloquent
{
	protected $table = 'user_socialnetwork';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'user_id' => 'int',
		'socialnetwork_id' => 'int'
	];
}
