<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Socialnetwork
 * 
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $key1
 * @property string $key2
 * @property bool $enabled
 *
 * @package App\Models\Base
 */
class Socialnetwork extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'enabled' => 'bool'
	];
}
