<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LoginAttempt
 * 
 * @property int $id
 * @property int $user_id
 * @property string $login_ip
 * @property \Carbon\Carbon $login_time
 *
 * @package App\Models\Base
 */
class LoginAttempt extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $dates = [
		'login_time'
	];
}
