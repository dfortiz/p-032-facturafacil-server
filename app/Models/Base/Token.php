<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Token
 * 
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property string $refresh_token
 * @property \Carbon\Carbon $expires_in
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class Token extends Eloquent
{
	protected $casts = [
		'user_id' => 'int'
	];

	protected $dates = [
		'expires_in'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
