<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cli
 * 
 * @property string $clinit
 * @property string $clinomb
 * @property string $clifrankdir
 * @property string $clitelf
 * @property string $clistat
 *
 * @package App\Models\Base
 */
class Cli extends Eloquent
{
	protected $table = 'cli';
	protected $primaryKey = 'clinit';
	public $incrementing = false;
	public $timestamps = false;
}
