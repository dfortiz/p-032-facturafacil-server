<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Enterprise
 * 
 * @property int $id
 * @property string $name
 * @property string $nit
 * @property string $from
 * @property string $from2
 * @property string $from3
 * @property string $from4
 * @property string $logo
 * @property string $ivback
 * @property string $template
 * @property int $state
 *
 * @package App\Models\Base
 */
class Enterprise extends Eloquent
{
	protected $table = 'enterprise';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'state' => 'int'
	];
}
