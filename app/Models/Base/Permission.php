<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Permission
 * 
 * @property int $id
 * @property string $slug
 *
 * @package App\Models\Base
 */
class Permission extends Eloquent
{
	public $timestamps = false;
}
