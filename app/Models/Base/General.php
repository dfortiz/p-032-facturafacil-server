<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class General
 * 
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $email
 * @property string $address
 * @property string $phone
 *
 * @package App\Models\Base
 */
class General extends Eloquent
{
	protected $table = 'general';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];
}
