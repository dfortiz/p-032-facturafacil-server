<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Invoicedetail
 * 
 * @property int $id
 * @property string $item
 * @property float $quantity
 * @property float $price
 * @property int $invoice_id
 *
 * @package App\Models\Base
 */
class Invoicedetail extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'quantity' => 'float',
		'price' => 'float',
		'invoice_id' => 'int'
	];
}
