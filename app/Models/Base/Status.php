<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Status
 * 
 * @property int $id
 * @property string $name
 * @property string $note
 *
 * @package App\Models\Base
 */
class Status extends Eloquent
{
	protected $table = 'status';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];
}
