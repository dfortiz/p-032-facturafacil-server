<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Log
 * 
 * @property int $id
 * @property string $description
 * @property string $details
 * @property \Carbon\Carbon $created_at
 *
 * @package App\Models\Base
 */
class Log extends Eloquent
{
	public $timestamps = false;
}
