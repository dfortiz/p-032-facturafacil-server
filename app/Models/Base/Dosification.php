<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Dosification
 * 
 * @property int $id
 * @property string $authorization
 * @property int $year
 * @property string $sectors
 * @property \Carbon\Carbon $dt_registration
 * @property \Carbon\Carbon $dt_start
 * @property \Carbon\Carbon $dt_end
 * @property \Carbon\Carbon $dt_limit
 * @property int $invoice_next
 * @property int $invoice_start
 * @property int $invoice_end
 * @property string $key
 * @property string $sin_msg1
 * @property string $sin_msg2
 * @property int $enterprise_id
 * @property int $branch_id
 * @property int $state
 *
 * @package App\Models\Base
 */
class Dosification extends Eloquent
{
	protected $table = 'dosification';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'year' => 'int',
		'invoice_next' => 'int',
		'invoice_start' => 'int',
		'invoice_end' => 'int',
		'enterprise_id' => 'int',
		'branch_id' => 'int',
		'state' => 'int'
	];

	protected $dates = [
		'dt_registration',
		'dt_start',
		'dt_end',
		'dt_limit'
	];
}
