<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Jun 2018 14:24:34 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $photo
 * @property string $phone
 * @property \Carbon\Carbon $birthday
 * @property string $address
 * @property bool $activated
 * @property string $salt
 * @property string $token
 * @property string $email_token
 * @property string $remember_token
 * @property string $register_ip
 * @property string $forget_slug
 * @property string $active_slug
 * @property \Carbon\Carbon $last_login
 * 
 * @property \Illuminate\Database\Eloquent\Collection $tokens
 *
 * @package App\Models\Base
 */
class User extends Eloquent
{
	protected $casts = [
		'activated' => 'bool'
	];

	protected $dates = [
		'birthday',
		'last_login'
	];

	public function tokens()
	{
		return $this->hasMany(\App\Models\Token::class);
	}
}
