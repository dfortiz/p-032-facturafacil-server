<?php

namespace App\Models;

class Token extends \App\Models\Base\Token
{
	protected $hidden = [
		'access_token',
		'refresh_token'
	];

	protected $fillable = [
		'user_id',
		'access_token',
		'refresh_token',
		'expires_in'
	];
}
