<?php

namespace App\Models;

class Parameter extends \App\Models\Base\Parameter
{
	protected $fillable = [
		'name',
		'value'
	];
}
