<?php

namespace App\Models;

class Invoicedetail extends \App\Models\Base\Invoicedetail
{
	protected $fillable = [
		'item',
		'quantity',
		'price',
		'invoice_id'
	];
}
