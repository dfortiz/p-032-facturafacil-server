<?php

namespace App\Models;

class UserPreference extends \App\Models\Base\UserPreference
{
	protected $fillable = [
		'name',
		'value'
	];
}
