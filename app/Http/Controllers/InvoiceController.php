<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dosification;
use App\Models\Invoice;
use App\Models\Invoicedetail;

use App\Libraries\ControlCode;
use App\Libraries\NumeroLiteral;

class InvoiceController extends Controller
{

    public function addinvoice(Request $request)
    {
    	$responseObj = array();
    	$invoiceObj = array();
    	$invoiceObj['name'] = $request->get('name');
    	$invoiceObj['nit'] = $request->get('nit');

 		$dosification = Dosification::find(1);
 		$dosification->invoice_next += 1;
 		$dosification->save();


 		$controlCode1 = new ControlCode();
 		$control_code = 'fdad';

	     $control_code = $controlCode1->generate
	     (
	     "7904006306693",//autorizacion
	     "876814",//Numero de factura
	     "1665979",//Nit
	     str_replace('/','','2008/05/19'),//fecha de transaccion de la forma AAAAMMDD
	     "35958,6",//Monto 
	     'zZ7Z]xssKqkEf_6K9uH(EcV+%x+u[Cca9T%+_$kiLjT8(zr3T9b5Fx2xG-D+_EBS'
	     );

    	$invoiceObj['cc'] = $control_code;


 		//$invoiceObj['itemsc'] = $request->get('items')[0]['itemname'];
        //$invoiceObj['itemcount'] = sizeof( $request->get('items') );
		
		$invoiceObj['items']=array();
        $totalamount = 0;
        for ( $i=0; $i < sizeof( $request->get('items') ); $i++ ) {
        	
			$invoiceItemObj = array();
    		$invoiceItemObj['name'] = $request->get('items')[$i]['itemname'];
    		$invoiceItemObj['price'] = $request->get('items')[$i]['price'];
    		$invoiceItemObj['quantity'] = $request->get('items')[$i]['quantity'];
    		$invoiceObj['items'][]=$invoiceItemObj; //array_push($invoiceObj['items'], $invoiceItemObj);
    		$totalamount += $invoiceItemObj['price'] * $invoiceItemObj['quantity'];
        }


        $invoice = new Invoice();
 		$invoice->id = $dosification->invoice_next - 1;
 		$invoice->enterprise_id = 1;

 		$invoice->authorization = 'authorization';
 		$invoice->dosification_id = 0;
 		$invoice->control_code = $control_code;
 		$invoice->dt_emision = date('Y-m-d H:i:s'); // $date = date('Y-m-d H:i:s');
 		$invoice->key = $dosification->key;
 		$invoice->customer_nit = $request->get('nit');
 		$invoice->customer_name = $request->get('name');
 		$invoice->amount = $totalamount;
 		$invoice->literal = NumeroLiteral::valorEnLetras($totalamount);

 		$invoice->sin_msg1 = $dosification->sin_msg1;
 		$invoice->sin_msg2 = $dosification->sin_msg2;
 		$invoice->user_id = 1;
 		$invoice->branch_id = 1;
 		$invoice->state = 1;

 		$facqr = sprintf("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",
        'empnit',
        'ipnfac',
        'ipnaut',
        'facfregfmt',
        $totalamount,
        $totalamount,
        $control_code,
        $request->get('nit'),
        0,
        0,
        0,
        0);
        $invoice->qr = $facqr ;

 		$invoice->save();

 		for ( $i=0; $i < sizeof( $request->get('items') ); $i++ ) {

    		$invoicedetail = new Invoicedetail();
    		$invoicedetail->item = $request->get('items')[$i]['itemname'];
    		$invoicedetail->quantity = $request->get('items')[$i]['quantity'];
    		$invoicedetail->price = $request->get('items')[$i]['price'];
    		$invoicedetail->invoice_id = $invoice->id;
    		$invoicedetail->save();

        }
		$responseObj['iid'] =  "" . $invoice->id;
		$responseObj['eid'] =  "1";
        //return response()->json($invoiceObj, 200);
        return response()->json($responseObj, 200);
        //return response()->json(['addinvoice1', 'Product2', 'Product 33'], 200);
    }


    public function listinvoice(Request $request)
    {
    	$responseObj = array();
    	
    	$invoices = Invoice::where('enterprise_id', '=', 1)
    				->orderBy('id', 'desc')
    				->get();

    	return response()->json($invoices, 200);

    }



/*


{
  "name": "Frank Ortiz",
  "nit": "3892324",
  "items": [
    {
      "itemname": "martillo",
      "price": 78,
      "quantity": 2
    },
    {
      "itemname": "alicate",
      "price": 25,
      "quantity": 4
    }
  ]
}
*/

}
