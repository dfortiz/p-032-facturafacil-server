<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
//use App\Models\Token;

use App\Http\Controllers\Trans\LoginTr;

class UserController extends Controller
{
    

	public function validateLogin(Request $request)
    {
    	/*$cli = new cli;
        $cli->clinit = '78901';
        $cli->clinomb = 'damir';
        $cli->clistat = 'stattt';
        $cli->save();*/

        // Auth::login(User::find($request->get('id')));

        $user = User::find(1);

        $logintr = new LoginTr();
        $logintr->access_token = 'access_token!@dk.com';
        $logintr->token_type = 'token_type!@dk.com';
        $logintr->expires_in = 'expires_in!@dk.com';
        $logintr->uid = 'uid!@dk.com';
        $logintr->username = 'username!@dk.com';
        $logintr->profile = 'profile!@dk.com';

        if ($user ) {
    		// return response($logintr,200);
    		//return response(['User: yes FOUND'  , 'Product 22', 'Product 33'],200);

    		return response()->json($logintr, 200);
        }

    	return response(['User: NOT FOUND'  , 'Product 22', 'Product 33'],200);
    }

}
