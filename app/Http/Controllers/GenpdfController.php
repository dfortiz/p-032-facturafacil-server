<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Invoice;
use App\Models\Invoicedetail;
use App\Models\Enterprise;
use App\Models\Dosification;
//use App\Models\Token;

use App\Http\Controllers\Trans\LoginTr;

//use Barryvdh\Dompdf\Facade as PDF;
//use Barryvdh\DomPDF\Facade as PDF;
//use PDF;
use Barryvdh\DomPDF\Facade as PDF;
use View;
use QrCode;

class GenpdfController extends Controller
{
    

	public function GetInvoice($iid, $eid, Request $request) // 80/1
    {

        $enterprise = Enterprise::find($eid);
        $invoice = Invoice::find($iid);
        
        //$model = User::where('votes', '>', 100)->firstOrFail();

        $data = array(
            'enterprise_name'=>$enterprise->name, 
            'customer_name'=>$invoice->customer_name, 
            'customer_nit'=>$invoice->customer_nit, 
            'iid'=>$iid, 
            'eid'=>$eid, 
            'dt_emision'=>$invoice->dt_emision, 
            'cc'=>'7B-F3-48-A8|damir frank|ok'
            );
        $html = View::make('pdf', ['data' => $data])->render();

        /*
        <img 
        src="{!!$message->embedData(QrCode::format('png')->generate('Embed me into an e-mail!'), 'QrCode.png', 'image/png')!!}">

        QrCode::generate('Make me into a QrCode!', '../public/qrcodes/qrcode.svg');
        
        */

        //QrCode::format('png')->generate('Embed me into an e-mail!', 'QrCodeFX.png');

        $pdf = PDF::loadHTML($html);
        return $pdf->stream();
        //return view('pdf', ['data' => $data]);
        //return $pdf->download('invoice.pdf');
    }

    public function GetInvoice2($iid, $eid, Request $request) // 80/1
    {

        ini_set('default_socket_timeout', 60);

        $enterprise = Enterprise::find($eid);
        $invoice = Invoice::find($iid);
        $dosification = Dosification::where('enterprise_id', '=', 1)
                                    ->where('state', '=', 1)
                                    ->firstOrFail();
        $invoicedetail = Invoicedetail::where('invoice_id', '=', $invoice->id)->get();
        

        $data = array(
            'enterprise_obj'=>$enterprise, 
            'invoice_obj'=>$invoice, 
            'invoicedetail_obj'=>$invoicedetail, 
            'dosification_obj'=>$dosification, 
            'enterprise_name'=>$enterprise->name, 
            'customer_name'=>$invoice->customer_name, 
            'customer_nit'=>$invoice->customer_nit, 
            'iid'=>$iid, 
            'eid'=>$eid, 
            'dt_emision'=>$invoice->dt_emision, 
            'options' => ['View', 'Download', 'Vector']
            //'cc'=>asset('images/ent/1.jpg')
            );
        //return view('pdf', ['data' => $data]);
        $html = View::make('pdf', ['data' => $data])->render();
        $pdf = PDF::loadHTML($html);
        return $pdf->stream();
        //return $pdf->download('' . $invoice->id . '_' . date('d-m-Y', strtotime( $invoice->dt_emision)) . '.pdf');
    }


//$message->embedData(QrCode::format('png')->generate('Embed me into an e-mail!'), 'QrCode.png', 'image/png')


    public function GetPdf_test() // Request $request
    {

/*

https://gist.github.com/alfredoem/c7a6fbcba33c57948132/revisions

{{{ $data['name'] }}}
*/

        $data = array('name'=>'Frank Ortiz', 'date'=>'1/29/15');
        $html = View::make('pdf', ['data' => $data])->render();

        $pdf = PDF::loadHTML($html);
        
        return $pdf->stream();
        //return $pdf->download('invoice.pdf');
    }

}
