<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cli;

class UtestController extends Controller
{
	public function index()
    {
    	$cli = new cli;

        $cli->clinit = '78901';
        $cli->clinomb = 'damir';
        $cli->clistat = 'stattt';
        $cli->save();

    	return response(['Product 1111', 'Product 22', 'Product 33'],200);
    }

    public function index2()
    {
        //Auth::guard('api')->user(); // instance of the logged user
        //Auth::guard('api')->check(); // if a user is authenticated
        //Auth::guard('api')->id(); // the id of the authenticated user

    	// return response(['index2 1111', 'Product 22', 'Product 33'],200);
        return response()->json(['index2 1111', 'Product 22', 'Product 33'], 200);
    }

    public function index3()
    {
        //Auth::guard('api')->user(); // instance of the logged user
        //Auth::guard('api')->check(); // if a user is authenticated
        //Auth::guard('api')->id(); // the id of the authenticated user

        return response(['index3:okokok', 'Product sss22'],200);
    }

/*
public function show(Article $article)
    {
        return $article;
    }

    public function store(Request $request)
    {
        $article = Article::create($request->all());

        return response()->json($article, 201);
    }

responses code:
200: OK. The standard success code and default option.
201: Object created. Useful for the store actions.
204: No content. When an action was executed successfully, but there is no content to return.
206: Partial content. Useful when you have to return a paginated list of resources.
400: Bad request. The standard option for requests that fail to pass validation.
401: Unauthorized. The user needs to be authenticated.
403: Forbidden. The user is authenticated, but does not have the permissions to perform an action.
404: Not found. This will be returned automatically by Laravel when the resource is not found.
500: Internal server error. Ideally you're not going to be explicitly returning this, but if something unexpected breaks, this is what your user is going to receive.
503: Service unavailable. Pretty self explanatory, but also another code that is not going to be returned explicitly by the application.

https://www.toptal.com/laravel/restful-laravel-api-tutorial
https://code.tutsplus.com/es/tutorials/build-a-react-app-with-laravel-restful-backend-part-1-laravel-5-api--cms-29442


*/

}
