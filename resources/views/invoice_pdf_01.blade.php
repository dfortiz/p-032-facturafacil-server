<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>FACTURA</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }

    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
        <td valign="top"><img src="" alt="" width="150"/></td>
        <td align="right">
            <h3>{{{ $data['enterprise_name'] }}}</h3>
            <pre>
                Company representative name
                Company address
                Tax ID
                phone
                fax
            </pre>
        </td>
    </tr>

  </table>

  <table width="100%">
    <tr>
        <td><strong>Nombre:</strong> {{{ $data['customer_name'] }}}</td>
        <td><strong>Nit:</strong> {{{ $data['customer_nit'] }}}</td>
    </tr>
    <tr>
        <td><strong>Fecha:</strong>  {{{ $data['dt_emision'] }}}   </td>
        <td><strong>To:</strong> {{{ $data['customer_name'] }}}</td>
    </tr>
  </table>

  <br/>

  <table width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th>#</th>
        <th>Item</th>
        <th>Cantidad</th>
        <th>Precio Unitario </th>
        <th>Total </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Playstation IV - Black</td>
        <td align="right">1</td>
        <td align="right">1400.00</td>
        <td align="right">1400.00</td>
      </tr>
      <tr>
          <th scope="row">1</th>
          <td>Metal Gear Solid - Phantom</td>
          <td align="right">1</td>
          <td align="right">105.00</td>
          <td align="right">105.00</td>
      </tr>
      <tr>
          <th scope="row">1</th>
          <td>Final Fantasy XV - Game</td>
          <td align="right">1</td>
          <td align="right">130.00</td>
          <td align="right">130.00</td>
      </tr>
    </tbody>

    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td align="right">Total Bs.</td>
            <td align="right" class="gray">Bs. 1929.3</td>
        </tr>
    </tfoot>
  </table>

  <table width="100%">
    <tr>
        <td><strong>From:</strong> Linblum - Barrio teatral {{{ $data['iid'] }}}   {{{ $data['eid'] }}}</td>
        <td><strong>To:</strong> {{{ $data['customer_name'] }}}</td>
    </tr>
  </table>

  <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(150)->generate($data['cc'])) !!} ">


</body>
</html>