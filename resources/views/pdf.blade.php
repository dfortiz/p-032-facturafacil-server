<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>FACTURA</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }

    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
        <td valign="top"><img src="/var/www/inv/public/images/ent/{{{ $data['enterprise_obj']->logo }}}" alt="_" width="150"/></td>
        <td align="right">
            <h3>{{{ $data['enterprise_obj']->name }}}</h3>
            <pre>
                {{{ $data['enterprise_obj']->from2 }}}
                {{{ $data['enterprise_obj']->from3 }}}
                {{{ $data['enterprise_obj']->from4 }}}
                Autorizacion: {{{ $data['dosification_obj']->authorization }}}
                {{{ $data['dosification_obj']->sectors }}}

            </pre>
        </td>
    </tr>

  </table>


  <table width="100%">
    <tr>
        <td><strong>Nombre:</strong> {{{ $data['invoice_obj']->customer_name }}} </td>
        <td><strong>Nro. Factura: </strong> {{{ $data['invoice_obj']->id }}}</td>
    </tr>
    <tr>
        <td><strong>Nit:</strong>  {{{ $data['invoice_obj']->customer_nit }}}  </td>
        <td><strong>Fecha:</strong> {{{ date('d/m/Y', strtotime( $data['invoice_obj']->dt_emision ) ) }}}</td>
    </tr>
  </table>

  <br/>


  <table width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th style="text-align: center;">#</th>
        <th style="text-align: center;">Item</th>
        <th style="text-align: center;">Cantidad</th>
        <th style="text-align: center;">Precio Unitario </th>
        <th style="text-align: center;">Total </th>
      </tr>
    </thead>
    <tbody>

@foreach($data['invoicedetail_obj'] as $id)
      <tr>
        <th scope="row">{{{ $loop->iteration }}}</th>
        <td>{{{ $id->item }}}</td>
        <td align="right">{{{ $id->quantity }}}</td>
        <td align="right">{{{ number_format((float) $id->price, 2, '.', '') }}}</td>
        <td align="right">{{{ number_format((float)$id->quantity * $id->price, 2, '.', '')  }}}</td>
      </tr>

@endforeach


    </tbody>

    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td align="right">Total Bs.</td>
            <td align="right" class="gray">Bs. {{{ number_format((float) $data['invoice_obj']->amount, 2, '.', '') }}}</td>
        </tr>
    </tfoot>
  </table>

  <table width="100%">
    <tr>
        <td><strong>Son:</strong> {{{ $data['invoice_obj']->literal }}} </td>
    </tr>
  </table>

  <img 
  src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(150)->generate( $data['invoice_obj']->qr )) !!} ">

  <table width="100%">
    <tr>
 <!--        <td><strong>Son:</strong> {{{ $data['invoice_obj']->literal }}} </td>
        <td>Fecha Límite de emisión: {{{  $data['dosification_obj']->dt_limit }}}</td>
 -->        
        <td>Fecha Límite de emisión: {{{  date('d/m/Y', strtotime($data['dosification_obj']->dt_limit) )  }}}</td>


    </tr>
  </table>

  <table width="100%">
    <tr style="text-align: center;">
        <td>{{{ $data['invoice_obj']->sin_msg1 }}} </td>
    </tr>    
    <tr style="text-align: center;">
        <td>{{{ $data['invoice_obj']->sin_msg2 }}} </td>
    </tr>
    <tr style="text-align: center;">
        <td>Gracias por su preferencia !!! </td>
    </tr>
  </table>

<!-- {{{ phpinfo() }}}
 -->
</body>
</html>